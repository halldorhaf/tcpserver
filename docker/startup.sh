#!/bin/bash

# -Xms350m -Xmx350m 
args=""


if [[ -n "${MARATHON_APP_RESOURCE_MEM}" ]]; then
    mem=$(echo ${MARATHON_APP_RESOURCE_MEM}|awk -F. '{ print $1 }')
    mem=$(expr $(expr $mem / 10) "*" 7)
    args="-Xms${mem}m -Xmx${mem}m "
fi

java -Dlog4j.configurationFile=log4j2.xml ${args} -jar tcpserver.jar 
