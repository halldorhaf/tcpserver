# Build instructions.

Need OpenJDK 10 and a recent Apache maven installed on the host, an idea for improvement would be to create a build container that has access to the parent docker daemon to build.

## To build:

```
mvn clean package assembly:single dockerfile:build dockerfile:push
```

To push the image to a repo the entry under build / pluginManagement / plugins (groupId=com.spotify) / repository needs to be changed to something that your docker user can push to, currently it is halldoraf/tcpserver.

For maven to login to docker a <server> block needs to be added to ~/.m2/settings.xml (under <servers>):

```
   <server>
	<id>docker.io</id>
	<username>halldoraf</username>
	<password>PASSWORD</password>
   </server>
```

To run the software outsode of a docker container execute::

```
java -jar target/tcpserver-1.0-SNAPSHOT-jar-with-dependencies.jar
```

This will bind by default to port 8080 listening on all addresses.

The docker container can be run directly, with the same port. A healthcheck is defined that will alert if the container is not healty. It uses a base image halldoraf/java:10 that is the base system + java. This is to shorten build times.

# Terraform

The terraform is under terraform/ but I did not manage to finish it due to time constraints.
Since I am not familiar with AWS to a great extend I decided to use AWS Fargate (I've used Azure container instances before), but it was a bit more complex than I would like just to deploy a container.

