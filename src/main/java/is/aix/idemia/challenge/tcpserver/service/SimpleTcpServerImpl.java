/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is.aix.idemia.challenge.tcpserver.service;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author hrh
 */
public class SimpleTcpServerImpl implements ITcpService {

    private final int port;
    // This should be configurable from the environment.
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);
    private static final Logger LOGGER = LogManager.getLogger(SimpleTcpServerImpl.class.getName());
    private ServerSocket socket;

    public SimpleTcpServerImpl(int port) {
        this.port = port;
    }

    @Override
    public void listen() {
        try {
            socket = new ServerSocket(port);
            while (true) {
                LOGGER.info("Waiting for client.");
                try {
                    Socket s = socket.accept();
                    executorService.submit(new SimpleTcpHandleMessage(s, new SimpleMessageHandler()));
                } catch (IOException ex) {
                    LOGGER.warn("Error accepting connection.", ex);
                }
            }
        } catch (IOException ex) {
            LOGGER.error(ex);
        }
    }

}
