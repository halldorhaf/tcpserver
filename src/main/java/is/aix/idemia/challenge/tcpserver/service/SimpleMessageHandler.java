/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is.aix.idemia.challenge.tcpserver.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import is.aix.idemia.challenge.tcpserver.domain.ServerMessage;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author hrh
 */
public class SimpleMessageHandler implements IMessageHandler {

    private static final Logger LOGGER = LogManager.getLogger(SimpleMessageHandler.class.getName());
    // [DD/MM/YYYY HH:MM] MESSAGE BODY
    private final String pattern = "\\[(.*)\\] (.*)";
    private final Pattern r = Pattern.compile(pattern);
    private final ObjectMapper mapper = new ObjectMapper();
    
    @Override
    public String handle(String message) {
        LOGGER.info("Handling message=" + message);
        if (message == null) {
            return "";
        }
        String output = "";
        Matcher m = r.matcher(message);
        if (m.find()) {
            String datetime = m.group(1);
            String body = m.group(2);
            LOGGER.info("Message matches pattern. datetime="+datetime+";body="+body+"");
            //LocalDate parsedDate = LocalDate.parse(datetime, dtf);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
            LocalDateTime ldt = LocalDateTime.parse(datetime, dtf);
            long timestamp = ldt.toEpochSecond(ZoneOffset.UTC);
            ServerMessage serverMessage = new ServerMessage();
            serverMessage.setTimestamp(timestamp);
            serverMessage.setMessage(body);
            try {
                serverMessage.setHostname(InetAddress.getLocalHost().getHostName());
            } catch (UnknownHostException ex) {
                LOGGER.warn("Unable to get hostname.");
                throw new RuntimeException("Unable to find hostname.", ex);
            }
            if (System.getenv().containsKey("container_name")) {
                serverMessage.setContainer(System.getenv("container_name"));
            } else {
                serverMessage.setContainer("unknown");
            }
            try {
                output = this.mapper.writeValueAsString(serverMessage);
                System.out.println(output);
            } catch (JsonProcessingException ex) {
                LOGGER.warn("Error parsing message to json.", ex);
            }
        } else {
            LOGGER.info("Message did not match pattern.");
        }
        return output;
    }
    
}
