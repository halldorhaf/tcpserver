/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is.aix.idemia.challenge.tcpserver.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author hrh
 */
public class SimpleTcpHandleMessage implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger(SimpleTcpHandleMessage.class.getName());
    private final Socket client;
    private final IMessageHandler handler;

    public SimpleTcpHandleMessage(Socket client, IMessageHandler handler) {
        this.client = client;
        this.handler = handler;
    }

    @Override
    public void run() {
        try {
            LOGGER.info("Handling connection.");
            // format: [DD/MM/YYYY HH:MM] MESSAGE BODY
            // Assume UTC timezone.
            var reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
            String input;
            try {
                input = reader.readLine();
                LOGGER.info("Reading next line.");
                var msg = this.handler.handle(input);
                LOGGER.info("Closing client connection.");
                var pw = new PrintWriter(client.getOutputStream(), true);
                pw.write(msg);
                pw.close();
                reader.close();
                if (!client.isClosed()) {
                    client.close();
                }
            } catch (Exception e) {
                LOGGER.info("Exception caught from handler.", e);
            }
            if (!client.isClosed()) {
                client.close();
            }
        } catch (IOException ex) {
            LOGGER.warn("Exception when closing client socket.", ex);
        }
    }

}
