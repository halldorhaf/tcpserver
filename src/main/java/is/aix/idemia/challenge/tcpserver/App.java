package is.aix.idemia.challenge.tcpserver;

import is.aix.idemia.challenge.tcpserver.service.ITcpService;
import is.aix.idemia.challenge.tcpserver.service.SimpleTcpServerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



/**
 * Hello world!
 *
 */
public class App 
{
    private static final Logger LOGGER = LogManager.getLogger(App.class.getName());
    public static void main( String[] args )
    {
        if (Boolean.getBoolean("DEBUG")) {
            dumpEnv();
        }
        
        int port = 8080; // default.
        
        if (System.getenv().containsKey("listen_port")) {
            port = Integer.parseInt(System.getenv().get("listen_port"));
        }
        
        if (System.getProperties().containsKey("listen.port")) {
            port = Integer.parseInt(System.getProperty("port"));
        }
        
        LOGGER.info( "Starting service on port=" + port);
        
        ITcpService service = new SimpleTcpServerImpl(port);
        service.listen();
    }
    
    private static void dumpEnv() {
        System.getenv().keySet().forEach((key) -> {
            LOGGER.info(key + "=" + System.getenv(key));
        });
    }
        
}
