/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is.aix.idemia.challenge.tcpserver.service;

/**
 *
 * @author hrh
 */
public interface IMessageHandler {
    public String handle(String message);
}
