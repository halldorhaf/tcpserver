/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package is.aix.idemia.challenge.tcpserver.domain;

import java.util.Objects;

/**
 *
 * @author hrh
 */
public class ServerMessage {
    private long timestamp;
    private String hostname;
    private String container;
    private String message;

    public ServerMessage() {
        this(0,"","","");
    }
    
    public ServerMessage(long timestamp, String hostname, String container, String message) {
        this.timestamp = timestamp;
        this.hostname = hostname;
        this.container = container;
        this.message = message;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (int) (this.timestamp ^ (this.timestamp >>> 32));
        hash = 83 * hash + Objects.hashCode(this.hostname);
        hash = 83 * hash + Objects.hashCode(this.container);
        hash = 83 * hash + Objects.hashCode(this.message);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ServerMessage other = (ServerMessage) obj;
        if (this.timestamp != other.timestamp) {
            return false;
        }
        if (!Objects.equals(this.hostname, other.hostname)) {
            return false;
        }
        if (!Objects.equals(this.container, other.container)) {
            return false;
        }
        if (!Objects.equals(this.message, other.message)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Output{" + "timestamp=" + timestamp + ", hostname=" + hostname + ", container=" + container + ", message=" + message + '}';
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
