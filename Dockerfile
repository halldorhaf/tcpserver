# Use our base image installed with OpenJDK 10. Install our app and run as non-root user.
FROM halldoraf/java:10

ENV DEBUG=false
ENV listen_port=8080
ENV container_name=tcpservice

ARG JAR_FILE=tcpserver-1.0-SNAPSHOT-jar-with-dependencies.jar

RUN mkdir /opt/tcpserver && \ 
groupadd tcpserver && useradd -g tcpserver tcpserver && \
chown -R tcpserver:tcpserver /opt/tcpserver
WORKDIR /opt/tcpserver

COPY docker/startup.sh startup.sh
COPY docker/healthcheck.sh healthcheck.sh
COPY target/$JAR_FILE  /opt/tcpserver
COPY src/main/resources/log4j2.xml /opt/tcpserver
RUN ln -s $JAR_FILE tcpserver.jar && chmod +x startup.sh

EXPOSE 8080
USER tcpserver
HEALTHCHECK --interval=15s --timeout=5s --start-period=5s --retries=3 CMD /bin/bash /opt/tcpserver/healthcheck.sh
CMD ["./startup.sh"]
