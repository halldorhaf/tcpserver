# Set up cloudwatch group and log stream and retain logs for 30 days
resource "aws_cloudwatch_log_group" "cb_log_group" {
  name              = "/ecs/tcpserver"
  retention_in_days = 7

  tags {
    Name = "tcpserver"
  }
}

resource "aws_cloudwatch_log_stream" "cb_log_stream" {
  name           = "tcpserver-stream"
  log_group_name = "${aws_cloudwatch_log_group.cb_log_group.name}"
}