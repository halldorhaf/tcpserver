
variable "aws_region" {
  description = "The AWS region things are created in"
  default     = "eu-central-1"
}

variable "az_count" {
  description = "Number of AZs to cover in a given region"
  default     = "3"
}

variable "app_image" {
  description = "Docker image to run in the ECS cluster"
  default     = "halldoraf/tcpserver:1.0-SNAPSHOT""
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 8080
}

variable "app_count" {
  description = "Number of docker containers to run"
  default     = 3
}

# Investigate - create automatically.
variable "ecs_autoscale_role" {
  description = "Role arn for the ecsAutocaleRole"
  default     = ""
}

# Investigate - create automatically.
variable "ecs_task_execution_role" {
  description = "Role arn for the ecsTaskExecutionRole"
  default     = ""
}

variable "health_check_path" {
  default = "/"
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "1024"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "2048"
}