[
  {
    "name": "tcpserver",
    "image": "${app_image}",
    "cpu": ${fargate_cpu},
    "memory": ${fargate_memory},
    "networkMode": "awsvpc",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/ecs/tcpserver",
          "awslogs-region": "${aws_region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "portMappings": [
      {
        "containerPort": ${app_port},
        "hostPort": ${app_port}
      }
    ],
    "healthCheck": {
                "retries": 3,
                "command": [
                    "CMD",
                    "/bin/bash",
                    "/opt/tcpserver/healthcheck.sh"
                ],
                "timeout": 5,
                "interval": 15,
                "startPeriod": 30
            }
  }
]
